import { Component, OnInit, AfterViewInit, ViewChild, ViewChildren, ElementRef, QueryList } from '@angular/core';
import * as faker from 'faker';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
    title = 'pdf-export-test';

    public categories: any[] = [];
    public scale = 0.5;

    @ViewChildren('category', { read: ElementRef }) rowCategories: QueryList<ElementRef>;
    @ViewChildren('subCategory', { read: ElementRef }) subCategories: QueryList<ElementRef>;
    @ViewChildren('header', { read: ElementRef }) headers: QueryList<ElementRef>;
    @ViewChildren('body', { read: ElementRef }) bodies: QueryList<ElementRef>;

    private rowCollection: ElementRef[];
    private nativeCategories: HTMLElement[];

    ngOnInit() {
        this.rowCollection = [];
        this.nativeCategories = [];

        for (var i = 0; i < 10; i++) {
            let currCode = faker.finance.currencyCode();
            let amount = faker.finance.amount();

            this.categories.push({
                account: faker.finance.account(),
                currency: currCode,
                value: amount,
                sub: this.getSubCategories()
            });
        }
    }

    ngAfterViewInit() {
        this.getRows();
    }

    private getAssets(): any[] {
        const assets = [];

        for (var i = 0; i < 35; i++) {
            let amount = faker.finance.amount();
            let amount2 = faker.finance.amount();
            let amount3 = faker.finance.amount();
            let amount4 = faker.finance.amount();
            let companyName = faker.company.companyName();
            let currCode = faker.finance.currencyCode();
            let percentage = Math.floor(1 + Math.random() * 10) + '%';

            assets.push({
                asset: companyName,
                value: amount,
                currency: currCode,
                marketValue: amount2,
                loan: percentage,
                collateral: amount3,
                collateral2: amount4,
            })
        }

        return assets
    }

    private getSubCategories(): any[] {
        const subCategories = [];

        for (var i = 0; i < 10; i++) {
            let currCode = faker.finance.currencyCode();
            let amount = faker.finance.amount();
            let type = faker.finance.transactionType()

            subCategories.push({
                trans: type,
                value: amount,
                currency: currCode,
                assets: this.getAssets(),
            })
        }

        return subCategories;
    }

    private getRows(): void {
        this.rowCategories.toArray().forEach(el => { this.rowCollection.push(el); this.nativeCategories.push(el.nativeElement) });
        this.subCategories.toArray().forEach(el => { this.rowCollection.push(el) });
        this.headers.toArray().forEach(el => { this.rowCollection.push(el) });
        this.bodies.toArray().forEach(el => { this.rowCollection.push(el) });

        this.breakRows();
    }

    private breakRows(): void {
        const sortedCollection = [...this.rowCollection].sort((a, b) => this.sortArray(a, b));
        let firstPageElement = sortedCollection[0].nativeElement;

        sortedCollection.filter(Boolean).forEach((currentElementRef, index) => {
            const currentNode = currentElementRef.nativeElement;
            const distanceFromFirstPageElement = Math.abs(firstPageElement.getBoundingClientRect().top - currentNode.getBoundingClientRect().bottom);
            const scaledPageHeight = 500 / this.scale;

            if (distanceFromFirstPageElement >= scaledPageHeight) {
                if (
                    this.rowCategories.toArray().includes(currentElementRef) ||
                    this.subCategories.toArray().includes(currentElementRef)
                ) {
                    firstPageElement = this.setNextFirstPageElement(currentElementRef);
                    this.addBreak(firstPageElement);
                } else if (this.headers.toArray().includes(currentElementRef)) {
                    firstPageElement = this.setNextFirstPageElement(sortedCollection[index - 1]);
                    this.addBreak(firstPageElement);
                } else {
                    if (this.headers.toArray().includes(sortedCollection[index - 1])) {
                        firstPageElement = this.setNextFirstPageElement(sortedCollection[index - 2]);
                        this.addBreak(firstPageElement);
                    } else {
                        firstPageElement = sortedCollection[index + 1].nativeElement;
                        this.addBreak(currentNode);
                    }
                }
            }
        });
    }

    private sortArray(a: ElementRef, b: ElementRef): number {
        if (a === b) {
            return 0;
        }

        if (!a.nativeElement.compareDocumentPosition) {
            return a.nativeElement.sourceIndex - b.nativeElement.sourceIndex;
        }

        if (a.nativeElement.compareDocumentPosition(b.nativeElement) === 2) {
            return 1;
        }
        return -1;
    }

    private addBreak(elem: HTMLElement) {
        if (elem) {
            elem.classList.add('page-break');
            elem.style.border = '5px solid red';
        }
    }

    private setNextFirstPageElement(elem: ElementRef): HTMLElement {
        let elemToReturn: HTMLElement;
        const previousElementSibling = elem.nativeElement.parentElement.previousElementSibling;

        if (this.nativeCategories.includes(previousElementSibling)) {
            elemToReturn = previousElementSibling
        } else {
            elemToReturn = elem.nativeElement;
        }
        
        return elemToReturn;
    }
}
