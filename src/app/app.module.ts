import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import { PdfFrameComponent } from './components/pdf-frame/pdf-frame.component';

@NgModule({
  declarations: [
    AppComponent,
    PdfFrameComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PDFExportModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
