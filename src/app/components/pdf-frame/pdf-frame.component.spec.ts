import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdfFrameComponent } from './pdf-frame.component';

describe('PdfFrameComponent', () => {
  let component: PdfFrameComponent;
  let fixture: ComponentFixture<PdfFrameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdfFrameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdfFrameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
