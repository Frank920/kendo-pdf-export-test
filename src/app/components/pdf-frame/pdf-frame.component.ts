import { Component, OnInit, ViewChild, Input } from '@angular/core';

@Component({
  selector: 'app-pdf-frame',
  templateUrl: './pdf-frame.component.html',
  styleUrls: ['./pdf-frame.component.scss']
})
export class PdfFrameComponent implements OnInit {

  @ViewChild('wrap', {static: true}) wrap;

  @Input()
  scale: number;

  constructor() { }

  ngOnInit(): void {
    if(this.scale) {
      this.wrap.nativeElement.style.setProperty('--scale', 1/ this.scale);
    }
  }

}
